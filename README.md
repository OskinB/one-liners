# Short jokes

Funny short jokes all in one place! HAHA

## Getting Started

To start contributing to this project you should fork it to your own GitLab repository. Then you can clone it to your local repository on your machine.

### Prerequisites

You will need to have Node.js on your machine and this document will assume that you have yarn (npm should be ok too). To install yarn do:

```
npm install -g yarn
```

you will also need to have jest (that is the testing framework: https://jestjs.io/docs/en/getting-started). To install it just run the yarn command in the terminal in your project directory with no arguments like so:

```
yarn
```

### How to contribute to the project

If you want to add a hilarious joke to the project you will have to edit the main.js file in the js folder. 
In the jokes array, add the joke question to the question object and the answer/punch-line in the answer object like so: 

```
var jokes = [
    {
        "question": "<h2>Helvetica and Times New Roman walk into a bar.</h2>",
        "answer": `<h3>- "Get out of here!" shouts the bartender. "We don't serve your type!"</h3>`
    },
    {
        "question": "<h2> Your question here <h2>",
        "answer": "<h3>- Your answer here <h3>"
    }
];

```
#### Tip
Just remember to add a comma after the previous line. 

And this is it. If you have added your joke and punch-line to the correct places you will have made a successful contribution to the project:)

## Running the tests

Before you commit your code and do a merge request you should test your code by typing:

```
yarn test
```

into your terminal. If everything is successful you should see something like:

```
Test Suites: 3 passed, 3 total
Tests:       6 passed, 6 total
Snapshots:   0 total
Time:        1.089s
Ran all test suites.
Done in 1.76s.
```

## Acknowledgments

* Thanks to my teachers, Pedro and Smári, for making my project come to live!